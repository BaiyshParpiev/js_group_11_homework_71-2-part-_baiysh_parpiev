import {axiosApi} from "./axiosApi";


export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_FAILURE = 'FETCH_DISHES_FAILURE';

export const FETCH_ORDERS_REQUEST = 'FETCH_ORDERS_REQUEST';
export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_ORDERS_FAILURE = 'FETCH_ORDERS_FAILURE';
export const ADD = 'ADD';
export const CLOSE = 'CLOSE';
export const OPEN = 'OPEN';
export const ORDER = 'ORDER';
export const CANCEL = 'CANCEL'

export const add = num => ({type: ADD, payload: num});
export const close = () => ({type: CLOSE});
export const open = () => ({type: OPEN});
export const order = (o, i) => ({type: ORDER, payloadO: o, payloadI:  i})
export const cancel = () => ({type: CANCEL})

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, payload: dishes});
export const fetchDishesFailure = error => ({type: FETCH_DISHES_FAILURE, payload: error});

export const fetchOrdersRequest = () => ({type: FETCH_ORDERS_REQUEST});
export const fetchOrdersSuccess = () => ({type: FETCH_ORDERS_SUCCESS});
export const fetchOrdersFailure = error => ({type: FETCH_ORDERS_FAILURE, payload: error});


export const fetchDishes = () => {
    return async dispatch => {
        try{
            dispatch(fetchDishesRequest());
            const response = await axiosApi.get('/dishes.json');
            dispatch(fetchDishesSuccess(response.data));
        }catch(error) {
            dispatch(fetchDishesFailure(error));
            throw error;
        }
    }
}

export const fetchOrders = data => {
    return async dispatch => {
        try{
            dispatch(fetchOrdersRequest());
            await axiosApi.post('/orders.json', data);
            dispatch(fetchOrdersSuccess());
        }catch(error) {
            dispatch(fetchOrdersFailure(error));
            throw error;
        }
    }
}




