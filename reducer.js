import {
    ADD,
    CANCEL,
    CLOSE,
    FETCH_DISHES_FAILURE,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS,
    OPEN,
    ORDER
} from "./actions";

const initialState = {
    dishes: null,
    loading: false,
    fetchLoading: false,
    error: null,
    price: 0,
    modal: false,
    orders: null,
}

const orders = (state, action) => {
    return {...state, orders: {...state.orders, [action.payloadI]: action.payloadO}};
}
export const reducer = (state = initialState, action) => {
    switch (action.type){
        case ADD:
            return {...state, price: state.price + action.payload};
        case CLOSE:
            return {...state, modal: false};
        case OPEN:
            return {...state, modal: true};
        case CANCEL:
            return {...state, orders: null}
        case ORDER:
            return orders(state, action)
        case FETCH_DISHES_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state, fetchLoading: false, dishes: action.payload};
        case FETCH_DISHES_FAILURE:
            return {...state, fetchLoading: false, error: action.payload};
        default:
            return state;
    }
}