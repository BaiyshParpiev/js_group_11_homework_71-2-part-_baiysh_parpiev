import React from 'react';
import Dishes from "./containers/Dishes/Dishes";
import {Provider} from "react-redux";
import {applyMiddleware, compose, createStore} from "redux";
import {reducer} from "./reducer";
import thunk from "redux-thunk";

export default function App() {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));
  return (
      <Provider store={store}>
        <Dishes/>
      </Provider>
  );
}

