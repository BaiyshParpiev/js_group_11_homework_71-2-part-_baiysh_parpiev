import React, {useEffect} from 'react';
import PlaceItem from "../../components/PlaceItem/PlaceItem";
import {ScrollView, StyleSheet, View, Text, Button} from "react-native";
import ModalDishes from "../Order/Order";
import {useDispatch, useSelector} from "react-redux";
import {add, fetchDishes, open, order} from "../../actions";

const Dishes = () => {
    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes);
    const price = useSelector(state => state.price);
    const modalShow = () => {
        dispatch(open())
    }
    useEffect(() => {
        dispatch(fetchDishes())
    }, [dispatch])

    const addOrder = i => {
        dispatch(order(dishes[i], i))
        const number = parseInt(dishes[i].price)
        dispatch(add(number))
    }


    return (
        <View style={styles.container}>
            <View style={styles.controls}>
                <Text style={styles.pizza}>Turtle Pizza</Text>
            </View>
            <ScrollView style={styles.placesList}>
                {dishes && Object.keys(dishes).map((place, i) => (
                    <PlaceItem key={i} onDelete={()=> addOrder(place)} placeName={dishes[place]}/>
                ))}
                <View><Text style={styles.price}>All price: {price}KGZ</Text></View>
            </ScrollView>
            <Button onPress={modalShow} title="Checkout"/>
            <ModalDishes/>
        </View>
    );
};

const styles = StyleSheet.create({
    placesList: {
        width: '100%',
        marginTop: 20,
        marginRight: 0,
    },
    price:{
        marginTop: 160,
        color: 'blue',
        width: '60%'
    },
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 20,
        paddingTop: 40,
    },
    pizza: {
        flex: 1,
        textAlign: 'center',
        fontSize: 25,
        borderBottomWidth: 3,
        borderBottomColor: 'blue',
        borderBottomRightRadius: 2,
        marginRight: 10,
    },
    controls: {
        flexDirection: 'row',
        padding: 20,
    },
});


export default Dishes;