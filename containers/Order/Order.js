import React from "react";
import {Alert, Modal, StyleSheet, Text, Pressable, View, Button} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import {cancel, close, fetchOrders} from "../../actions";

const ModalDishes = () => {
    const dispatch = useDispatch();
    const modalVisible = useSelector(state => state.modal);
    const order = useSelector(state => state.orders);


    const orderFoods = () => {
        dispatch(fetchOrders(order));
        dispatch(close())
    };

    const onCancel = () => {
        dispatch(cancel())
        dispatch(close())
    }
    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    dispatch(close());
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        {order && Object.keys(order).map((m, i) => {
                                return <Text key={i} style={styles.textStyle}>Name:{order[m].name} Price:{order[m].price}</Text>
                            })}
                        <Button style={styles.buttonOpen} onPress={onCancel} title="Cancel"/>
                        <Button style={styles.buttonClose} onPress={orderFoods} title="Order"/>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },
    modalView: {
        margin: 20,
        width: '80%',
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonOpen: {
        backgroundColor: "#F194FF",
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "black",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});

export default ModalDishes;