import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Image} from "react-native";

const PlaceItem = ({placeName, onDelete}) => {
    return (
        <TouchableOpacity onPress={onDelete} style={styles.place}>
            <View style={styles.container}>
                <Image
                    style={styles.stretch}
                    source={placeName.img}
                />
            </View>
            <View><Text>Name: {placeName.name}</Text></View>
            <View><Text>Price: {placeName.price}</Text></View>
        </TouchableOpacity>
    );
};


const styles = StyleSheet.create({
    place: {
        display: 'flex',
        width: '100%',
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#bbb'
    },
    stretch: {
        width: 50,
        height: 40,
    }
});

export default PlaceItem;